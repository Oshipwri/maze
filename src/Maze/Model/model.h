#ifndef MODEL_H
#define MODEL_H

#include <utility>
#include <vector>

#include "caveGenerator/cave_generator.h"
#include "mazeGenerator/maze_generator.h"

namespace s21 {
class Model {
 public:
  Model();
  void SetMazeVerticalSize(int size) { maze_.SetVerticalSize(size); };
  void SetMazeHorizontalSize(int size) { maze_.SetHorizontalSize(size); };
  void SetCaveVerticalSize(int size) { cave_.SetVerticalSize(size); };
  void SetCaveHorizontalSize(int size) { cave_.SetHorizontalSize(size); };
  void SetBornLimit(int limit) { cave_.SetBornLimit(limit); };
  void SetDeathLimit(int limit) { cave_.SetDeathLimit(limit); };
  void SetInitChance(int chance) { cave_.SetInitChance(chance); };
  void GenerateCave() { cave_.GenerateRandomField(); };
  void GenerateCaveNextIteration() { cave_.GenerateNextIteration(); };
  void GenerateMaze() { maze_.GenerateMaze(); };
  void SaveCaveField(const std::string& filepath) {
    cave_.SaveCaveField(filepath);
  };
  void LoadCaveField(const std::string& filepath) {
    cave_.OpenCaveField(filepath);
  };
  void SaveMazeField(const std::string& filepath) {
    maze_.SaveMazeField(filepath);
  };
  void LoadMazeField(const std::string& filepath) {
    maze_.LoadMazeField(filepath);
  };
  int GetMazeVerticalSize();
  int GetMazeHorizontalSize();
  int GetCaveVerticalSize();
  int GetCaveHorizontalSize();
  S21Matrix GetCaveField();
  bool IsCaveStaticState() { return cave_.GetIsStaticState(); };
  std::pair<S21Matrix, S21Matrix> GetMazeField();
  std::vector<std::pair<int, int>> GetMazeWay(int start_row, int start_col,
                                              int finish_row, int finish_col) {
    return maze_.GetMazeWay(start_row, start_col, finish_row, finish_col);
  }

 private:
  CaveGenerator cave_;
  MazeGenerator maze_;
};
}  // namespace s21

#endif  // MODEL_H
