#ifndef SRC_MAZE_MODEL_MATRIX_READER_WRITER_MATRIX_READER_WRITER_H_
#define SRC_MAZE_MODEL_MATRIX_READER_WRITER_MATRIX_READER_WRITER_H_

#include <fstream>
#include <iostream>

#include "../matrix/s21_matrix_oop.h"

namespace s21 {
class MatrixReaderWriter {
 public:
  MatrixReaderWriter(){};
  ~MatrixReaderWriter(){};

  void ReadFile(const std::string &filepath, S21Matrix &matrix);
  void ReadFile(const std::string &filepath, S21Matrix &matrix1,
                S21Matrix &matrix2);
  void WriteFile(const std::string &filepath, S21Matrix &matrix);
  void WriteFile(const std::string &filepath, S21Matrix &matrix1,
                 S21Matrix &matrix2);

 private:
  void WriteMatrix(const std::string &filepath, S21Matrix &matrix);
  void ReadMatrix(std::fstream *data, S21Matrix &matrix);
  static constexpr int kMaximalSize = 50;
  static constexpr int kMimimalSize = 2;
};
}  // namespace s21
#endif  // SRC_MAZE_MODEL_MATRIX_READER_WRITER_MATRIX_READER_WRITER_H_
