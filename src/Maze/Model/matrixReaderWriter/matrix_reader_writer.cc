#include "matrix_reader_writer.h"
namespace s21 {
void MatrixReaderWriter::WriteMatrix(const std::string &filepath,
                                     S21Matrix &matrix) {
  std::ofstream out;
  out.open(filepath, std::ios::app);
  for (int i = 0; i < matrix.get_rows(); i++) {
    for (int j = 0; j < matrix.get_cols(); j++) {
      if (matrix(i, j) == 0)
        out << matrix(i, j);
      else
        out << 1;
      if (j != matrix.get_cols() - 1) out << " ";
    }
    out << std::endl;
  }
  out.close();
}

void MatrixReaderWriter::WriteFile(const std::string &filepath,
                                   S21Matrix &matrix) {
  std::ofstream out;
  out.open(filepath);
  out << matrix.get_rows() << " " << matrix.get_cols() << std::endl;
  out.close();
  WriteMatrix(filepath, matrix);
}

void MatrixReaderWriter::WriteFile(const std::string &filepath,
                                   S21Matrix &matrix1, S21Matrix &matrix2) {
  if (matrix1.get_rows() != matrix2.get_rows() ||
      matrix1.get_cols() != matrix2.get_cols()) {
    throw std::out_of_range("Rows and cols must be equal in matrices");
  }
  WriteFile(filepath, matrix1);
  std::ofstream out;
  out.open(filepath, std::ios::app);
  out << std::endl;
  out.close();
  WriteMatrix(filepath, matrix2);
}

void MatrixReaderWriter::ReadMatrix(std::fstream *data, S21Matrix &matrix) {
  double current;
  for (int i = 0; i < matrix.get_rows(); i++) {
    for (int j = 0; j < matrix.get_cols(); j++) {
      *data >> current;
      if (current == 0)
        matrix(i, j) = current;
      else
        matrix(i, j) = 1;
    }
  }
}

void MatrixReaderWriter::ReadFile(const std::string &filepath,
                                  S21Matrix &matrix) {
  std::fstream data(filepath, std::ios_base::in);
  double current;
  data >> current;
  current = current > kMaximalSize ? kMaximalSize : current;
  matrix.set_rows(current);
  data >> current;
  current = current > kMaximalSize ? kMaximalSize : current;
  matrix.set_cols(current);
  ReadMatrix(&data, matrix);
  data.close();
}

void MatrixReaderWriter::ReadFile(const std::string &filepath,
                                  S21Matrix &matrix1, S21Matrix &matrix2) {
  std::fstream data(filepath, std::ios_base::in);
  double current_rows, current_cols;
  data >> current_rows;
  data >> current_cols;
  if (current_rows < kMimimalSize || current_cols < kMimimalSize)
    throw std::out_of_range("Incorrect matrix size");
  current_rows = current_rows > kMaximalSize ? kMaximalSize : current_rows;
  matrix1.set_rows(current_rows);
  matrix2.set_rows(current_rows);
  current_cols = current_cols > kMaximalSize ? kMaximalSize : current_cols;
  matrix1.set_cols(current_cols);
  matrix2.set_cols(current_cols);
  ReadMatrix(&data, matrix1);
  ReadMatrix(&data, matrix2);
  data.close();
}
}  // namespace s21
