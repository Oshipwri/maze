#include "matrix_reader_writer.h"

#include <gtest/gtest.h>

TEST(ReadWriteMatrix, OneMatrix) {
  s21::S21Matrix M1, M2;
  M1(0, 0) = 1;
  M1(1, 1) = 1;
  M1(2, 2) = 1;

  s21::MatrixReaderWriter mrw;
  mrw.WriteFile("./Maze/Model/matrixReaderWriter/file1.txt", M1);

  s21::S21Matrix M3;
  mrw.ReadFile("./Maze/Model/matrixReaderWriter/file1.txt", M3);
  for (int i = 0; i < M3.get_rows(); i++)
    for (int j = 0; j < M3.get_cols(); j++) {
      ASSERT_EQ(M1(i, j), M3(i, j));
    }

  M2.set_rows(5);
  M2.set_cols(7);
  M2(0, 0) = 1;
  M2(1, 1) = 1;
  M2(2, 2) = 1;
  M2(3, 3) = 1;
  M2(4, 4) = 1;
  mrw.WriteFile("./Maze/Model/matrixReaderWriter/file2.txt", M2);

  s21::S21Matrix M4;
  mrw.ReadFile("./Maze/Model/matrixReaderWriter/file2.txt", M4);
  ASSERT_EQ(M2.get_cols(), M4.get_cols());
  ASSERT_EQ(M2.get_rows(), M4.get_rows());
  for (int i = 0; i < M4.get_rows(); i++)
    for (int j = 0; j < M4.get_cols(); j++) {
      ASSERT_EQ(M4(i, j), M2(i, j));
    }
}

TEST(ReadWriteMatrix, TwoMatrices) {
  s21::S21Matrix M1, M2;
  s21::MatrixReaderWriter mrw;
  M1.set_rows(4);
  M2.set_rows(4);
  M1.set_cols(4);
  M2.set_cols(4);
  M1(0, 0) = 1;
  M1(0, 3) = 1;
  M1(1, 1) = 1;
  M1(1, 2) = 1;
  M1(1, 3) = 1;
  M1(2, 3) = 1;
  M1(3, 3) = 1;

  M2(0, 2) = 1;
  M2(0, 3) = 1;
  M2(1, 1) = 1;
  M2(2, 0) = 1;
  M2(2, 1) = 1;
  M2(2, 2) = 1;
  M2(3, 0) = 1;
  M2(3, 1) = 1;
  M2(3, 2) = 1;
  M2(3, 3) = 1;
  mrw.WriteFile("./Maze/Model/matrixReaderWriter/file1.txt", M1, M2);

  s21::S21Matrix M3, M4;
  mrw.ReadFile("./Maze/Model/matrixReaderWriter/file1.txt", M3, M4);
  for (int i = 0; i < M4.get_rows(); i++)
    for (int j = 0; j < M4.get_cols(); j++) {
      ASSERT_EQ(M1(i, j), M3(i, j));
      ASSERT_EQ(M2(i, j), M4(i, j));
    }
}
