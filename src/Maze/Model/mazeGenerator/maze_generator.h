#ifndef SRC_MAZE_MODEL_MAZE_GENERATOR_MAZE_GENERATOR_H_
#define SRC_MAZE_MODEL_MAZE_GENERATOR_MAZE_GENERATOR_H_

#include <stdio.h>

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <random>
#include <string>

#include "../matrixReaderWriter/matrix_reader_writer.h"

namespace s21 {
class MazeGenerator {
 public:
  MazeGenerator();
  ~MazeGenerator(){};
  int GetVerticalSize();
  int GetHorizontalSize();
  void SetVerticalSize(int size);
  void SetHorizontalSize(int size);
  int SumHorizontalWalls(int);
  S21Matrix GetVerticalMatrix();
  S21Matrix GetHorizontalMatrix();
  void SetMazeHorizontalSize(int size);
  void SetMazeVerticalSize(int size);
  void GenerateMaze();
  void SaveMazeField(const std::string& filepath);
  void LoadMazeField(const std::string& filepath);
  std::vector<std::pair<int, int>> GetMazeWay(int start_row, int start_col,
                                              int finish_row, int finish_col);
  void IterateNextWayStep(S21Matrix& way_matrix, int* current_step);
  void ChangeNeighbors(S21Matrix& way_matrix, int row, int col);
  std::vector<std::pair<int, int>> FindWay(S21Matrix& wave_matrix,
                                           int finish_row, int finish_col);

 private:
  MatrixReaderWriter matrixReaderWriter;
  int current_index_row_{0};
  int counter_{1};
  int rows_{10};
  int cols_{10};
  S21Matrix matrix_sets_;
  S21Matrix matrix_right_;
  S21Matrix matrix_bottom_;
  int NumberElementsInSet(int);
  void Clean();
  void CheckValid();
  bool RandomBoolean();
  void AssignSet();
  void InitMatrices();
  void MergeSet(int, int);
  void AddingEndLine();
  void PreparatingNewLine();
  void AddingVerticalWalls();
  void AddingHorizontalWalls();
  void CheckedHorizontalWalls();
  static constexpr int kBorder = 1;
  static constexpr int kIsEmpty = 0;
  static constexpr int kMinisFieldSize = 2;
  static constexpr int kMaxFieldSize = 50;
  static constexpr int kFirstStep = 1;
};
}  // namespace s21

#endif  // SRC_MAZE_MODEL_MAZE_GENERATOR_MAZE_GENERATOR_H_
