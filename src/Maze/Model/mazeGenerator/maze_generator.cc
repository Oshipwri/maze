#include "maze_generator.h"

namespace s21 {

MazeGenerator::MazeGenerator() { GenerateMaze(); }

/**
 * @brief initializes empty matrices
 *
 * @param VerticalSize number of rows
 * @param HorizontalSize number of cols
 */
void MazeGenerator::InitMatrices() {
  matrix_sets_ = S21Matrix(rows_, cols_);
  matrix_right_ = S21Matrix(rows_, cols_);
  matrix_bottom_ = S21Matrix(rows_, cols_);
}

/**
 * @brief main function of algorithm
 */
void MazeGenerator::GenerateMaze() {
  CheckValid();
  srand(time(0));
  InitMatrices();
  while (current_index_row_ < rows_) {
    AssignSet();
    AddingVerticalWalls();
    /**
     * @brief break the cycle on the last line of the matrix
     * according to the algorithm
     */
    if (current_index_row_ == rows_ - 1) {
      AddingEndLine();
      break;
    }
    AddingHorizontalWalls();
    CheckedHorizontalWalls();
    PreparatingNewLine();
    current_index_row_++;
  }
  Clean();
}

/**
 * @brief assigning a unique set
 */
void MazeGenerator::AssignSet() {
  for (int i = 0; i < matrix_sets_.get_cols(); i++) {
    if (matrix_sets_(current_index_row_, i) == kIsEmpty) {
      matrix_sets_(current_index_row_, i) = counter_;
      counter_++;
    }
  }
}

void MazeGenerator::AddingVerticalWalls() {
  for (int i = 0; i < matrix_sets_.get_cols(); i++) {
    if (i == matrix_sets_.get_cols() - 1) {
      matrix_right_(current_index_row_, i) = kBorder;
      continue;
    }
    bool selection = RandomBoolean();
    if (matrix_sets_(current_index_row_, i) ==
            matrix_sets_(current_index_row_, i + 1) ||
        selection) {
      matrix_right_(current_index_row_, i) = kBorder;
    } else {
      MergeSet(i, matrix_sets_(current_index_row_, i));
    }
  }
}

/**
 * @brief combining sets
 *
 * @param i index of current set
 * @param element value of the set
 */
void MazeGenerator::MergeSet(int i, int element) {
  int mutableSet = matrix_sets_(current_index_row_, i + 1);
  for (int j = 0; j < matrix_sets_.get_cols(); j++) {
    if (matrix_sets_(current_index_row_, j) == mutableSet) {
      matrix_sets_(current_index_row_, j) = element;
    }
  }
}

bool MazeGenerator::RandomBoolean() { return std::rand() % 2 == 1; }

void MazeGenerator::AddingHorizontalWalls() {
  for (int i = 0; i < matrix_sets_.get_cols(); i++) {
    bool selection = RandomBoolean();
    if (selection &&
        NumberElementsInSet(matrix_sets_(current_index_row_, i)) > 1) {
      matrix_bottom_(current_index_row_, i) = kBorder;
    }
  }
}

int MazeGenerator::NumberElementsInSet(int number_set) {
  int count = 0;
  for (int i = 0; i < matrix_sets_.get_cols() - 1; i++) {
    if (matrix_sets_(current_index_row_, i) == number_set) {
      count++;
    }
  }
  return count;
}

/**
 * @brief checking the condition that every set
 * has at least one cell without a bottom border
 */
void MazeGenerator::CheckedHorizontalWalls() {
  int current_set;
  for (int i = 0; i < matrix_sets_.get_cols(); i++) {
    current_set = matrix_sets_(current_index_row_, i);
    if (SumHorizontalWalls(current_set) < 1) {
      matrix_bottom_(current_index_row_, i) = kIsEmpty;
    }
  }
}

int MazeGenerator::SumHorizontalWalls(int element) {
  int counter = 0;
  for (int i = 0; i < matrix_sets_.get_cols(); i++) {
    if (matrix_sets_(current_index_row_, i) == element &&
        matrix_bottom_(current_index_row_, i) == !kBorder) {
      counter++;
    }
  }
  return counter;
}

void MazeGenerator::PreparatingNewLine() {
  for (int i = 0; i < matrix_sets_.get_cols(); i++) {
    if (matrix_bottom_(current_index_row_, i)) {
      matrix_sets_(current_index_row_ + 1, i) = kIsEmpty;
    } else {
      matrix_sets_(current_index_row_ + 1, i) =
          matrix_sets_(current_index_row_, i);
    }
  }
}

/**
 * @brief completes the last line of the maze
 */
void MazeGenerator::AddingEndLine() {
  for (int i = 0; i < matrix_sets_.get_cols(); i++) {
    matrix_bottom_(current_index_row_, i) = kBorder;
    if (i == matrix_sets_.get_cols() - 1) continue;
    if (matrix_sets_(current_index_row_, i) !=
        matrix_sets_(current_index_row_, i + 1)) {
      matrix_right_(current_index_row_, i) = kIsEmpty;
      MergeSet(i, matrix_sets_(current_index_row_, i));
    }
  }
  matrix_right_(current_index_row_, matrix_right_.get_cols() - 1) = kBorder;
}

int MazeGenerator::GetVerticalSize() { return matrix_right_.get_rows(); }

int MazeGenerator::GetHorizontalSize() { return matrix_right_.get_cols(); }

S21Matrix MazeGenerator::GetVerticalMatrix() { return matrix_right_; }

S21Matrix MazeGenerator::GetHorizontalMatrix() { return matrix_bottom_; }

void MazeGenerator::CheckValid() {
  if (rows_ < kMinisFieldSize || rows_ > kMaxFieldSize ||
      cols_ < kMinisFieldSize || cols_ > kMaxFieldSize) {
    throw std::invalid_argument("invalid field size");
  }
}

void MazeGenerator::Clean() {
  current_index_row_ = 0;
  counter_ = 1;
  rows_ = cols_ = 10;
}

void MazeGenerator::SetVerticalSize(int size) { rows_ = size; }

void MazeGenerator::SetHorizontalSize(int size) { cols_ = size; }

/**
 * @brief The function of writing to the file of the created maze. Throws an
 * exception if the specified path is not correct
 *
 * @param filepath file path
 */
void MazeGenerator::SaveMazeField(const std::string& filepath) {
  matrixReaderWriter.WriteFile(filepath, matrix_right_, matrix_bottom_);
}

void MazeGenerator::LoadMazeField(const std::string& filepath) {
  matrixReaderWriter.ReadFile(filepath, matrix_right_, matrix_bottom_);
  SetVerticalSize(matrix_right_.get_rows());
  SetHorizontalSize(matrix_right_.get_cols());
}

std::vector<std::pair<int, int>> MazeGenerator::GetMazeWay(int start_row,
                                                           int start_col,
                                                           int finish_row,
                                                           int finish_col) {
  S21Matrix wave_matrix(GetVerticalSize(), GetHorizontalSize());
  int current_step = kFirstStep;
  wave_matrix(start_row, start_col) = current_step;
  bool is_finish = false;
  while (!is_finish) {
    S21Matrix wave_copy(wave_matrix);
    IterateNextWayStep(wave_matrix, &current_step);
    current_step++;
    if (wave_matrix(finish_row, finish_col) != 0 || wave_matrix == wave_copy) {
      is_finish = true;
    }
  }

  return FindWay(wave_matrix, finish_row, finish_col);
}

std::vector<std::pair<int, int>> MazeGenerator::FindWay(S21Matrix& wave_matrix,
                                                        int finish_row,
                                                        int finish_col) {
  std::vector<std::pair<int, int>> way;
  int steps = wave_matrix(finish_row, finish_col);
  int current_row = finish_row;
  int current_col = finish_col;
  if (steps) {
    way.push_back(std::pair(finish_row, finish_col));
    while (steps > kFirstStep) {
      int next_step = steps - 1;
      if (current_col > 0 &&
          wave_matrix(current_row, current_col - 1) == next_step &&
          matrix_right_(current_row, current_col - 1) == 0) {
        current_col--;
      } else if (current_row > 0 &&
                 wave_matrix(current_row - 1, current_col) == next_step &&
                 matrix_bottom_(current_row - 1, current_col) == 0) {
        current_row--;
      } else if (current_col < wave_matrix.get_cols() - 1 &&
                 wave_matrix(current_row, current_col + 1) == next_step &&
                 matrix_right_(current_row, current_col) == 0) {
        current_col++;
      } else if (current_row < wave_matrix.get_rows() - 1 &&
                 wave_matrix(current_row + 1, current_col) == next_step &&
                 matrix_bottom_(current_row, current_col) == 0) {
        current_row++;
      }
      way.push_back(std::pair(current_row, current_col));
      steps = next_step;
    }
  }
  return way;
}

void MazeGenerator::IterateNextWayStep(S21Matrix& way_matrix,
                                       int* current_step) {
  for (int i = 0; i < way_matrix.get_rows(); i++) {
    for (int j = 0; j < way_matrix.get_cols(); j++) {
      if (way_matrix(i, j) == *current_step) {
        ChangeNeighbors(way_matrix, i, j);
      }
    }
  }
}

void MazeGenerator::ChangeNeighbors(S21Matrix& way_matrix, int row, int col) {
  if (col > 0 && matrix_right_(row, col - 1) == 0 &&
      way_matrix(row, col - 1) == 0)
    way_matrix(row, col - 1) = way_matrix(row, col) + 1;
  if (row > 0 && matrix_bottom_(row - 1, col) == 0 &&
      way_matrix(row - 1, col) == 0)
    way_matrix(row - 1, col) = way_matrix(row, col) + 1;
  if (col < way_matrix.get_cols() - 1 && matrix_right_(row, col) == 0 &&
      way_matrix(row, col + 1) == 0)
    way_matrix(row, col + 1) = way_matrix(row, col) + 1;
  if (row < way_matrix.get_rows() - 1 && matrix_bottom_(row, col) == 0 &&
      way_matrix(row + 1, col) == 0)
    way_matrix(row + 1, col) = way_matrix(row, col) + 1;
}

}  // namespace s21
