#include <gtest/gtest.h>

#include "maze_generator.h"

TEST(Maze, Generate) {
  s21::MazeGenerator maze;
  int rows = 5;
  int cols = 50;
  maze.SetVerticalSize(rows);
  maze.SetHorizontalSize(cols);
  maze.GenerateMaze();
  ASSERT_EQ(maze.GetVerticalSize(), rows);
  ASSERT_EQ(maze.GetHorizontalSize(), cols);
}

TEST(Maze, GetMazeMatrix) {
  s21::MazeGenerator maze;
  int rows = 15;
  int cols = 20;
  maze.SetVerticalSize(rows);
  maze.SetHorizontalSize(cols);
  maze.GenerateMaze();
  s21::S21Matrix vertical = maze.GetVerticalMatrix();
  ASSERT_EQ(vertical.get_rows(), rows);
  ASSERT_EQ(vertical.get_cols(), cols);
  s21::S21Matrix horizontal = maze.GetHorizontalMatrix();
  ASSERT_EQ(horizontal.get_rows(), rows);
  ASSERT_EQ(horizontal.get_cols(), cols);
}

TEST(Maze, ValidData) {
  s21::MazeGenerator maze;
  int rows = -5;
  maze.SetVerticalSize(rows);
  ASSERT_ANY_THROW(maze.GenerateMaze());
}

TEST(Maze, LoadSave) {
  std::string filepath = "./Maze/Model/mazeGenerator/test_load_save.txt";
  s21::MazeGenerator maze;
  int rows = 15;
  int cols = 20;
  maze.SetVerticalSize(rows);
  maze.SetHorizontalSize(cols);
  maze.GenerateMaze();
  maze.SaveMazeField(filepath);
  maze.LoadMazeField(filepath);
  ASSERT_EQ(maze.GetVerticalSize(), rows);
  ASSERT_EQ(maze.GetHorizontalSize(), cols);
}

TEST(Maze, FindWay) {
  s21::MazeGenerator maze;
  std::string filepath = "./Maze/Model/mazeGenerator/test_way.txt";
  int rows = 3;
  int cols = 3;
  int way_len = 9;
  int start_x = 0, start_y = 0;
  int end_x = 2, end_y = 2;
  maze.LoadMazeField(filepath);
  ASSERT_EQ(maze.GetVerticalSize(), rows);
  ASSERT_EQ(maze.GetHorizontalSize(), cols);
  auto way = maze.GetMazeWay(start_x, start_y, end_x, end_y);
  ASSERT_EQ(way.size(), way_len);
  ASSERT_EQ(way[0].first, end_x);
  ASSERT_EQ(way[0].second, end_y);
  ASSERT_EQ(way[way_len - 1].first, start_x);
  ASSERT_EQ(way[way_len - 1].second, start_y);
}