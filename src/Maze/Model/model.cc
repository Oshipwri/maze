#include "model.h"

namespace s21 {

Model::Model() {}

std::pair<S21Matrix, S21Matrix> Model::GetMazeField() {
  return std::make_pair(maze_.GetVerticalMatrix(), maze_.GetHorizontalMatrix());
}

int Model::GetMazeVerticalSize() { return maze_.GetVerticalSize(); }

int Model::GetMazeHorizontalSize() { return maze_.GetHorizontalSize(); }

int Model::GetCaveVerticalSize() { return cave_.GetVerticalSize(); }

int Model::GetCaveHorizontalSize() { return cave_.GetHorizontalSize(); }

S21Matrix Model::GetCaveField() { return cave_.GetCaveField(); }

}  // namespace s21
