#ifndef SRC_MAZE_MODEL_CAVE_GENERATOR_CAVE_GENERATOR_H_
#define SRC_MAZE_MODEL_CAVE_GENERATOR_CAVE_GENERATOR_H_

#include "../matrixReaderWriter/matrix_reader_writer.h"
namespace s21 {
class CaveGenerator {
 public:
  CaveGenerator();
  ~CaveGenerator(){};

  int GetBornLimit();
  int GetDeathLimit();

  void SetBornLimit(int limit);
  void SetDeathLimit(int limit);

  void OpenCaveField(const std::string& filepath);
  void SaveCaveField(const std::string& filepath);

  void GenerateNextIteration();
  void FindNextCellState(S21Matrix& cave_copy, int row, int col);
  void GenerateRandomField();
  int GenerateDeadOrAlive();

  void SetVerticalSize(int size);
  void SetHorizontalSize(int size);
  int GetVerticalSize();
  int GetHorizontalSize();

  void SetInitChance(int chance);
  int GetInitChance();

  bool GetIsStaticState();

  S21Matrix GetCaveField() { return cave_field_; };

 private:
  int born_limit_;
  int death_limit_;
  int init_chance_;
  bool is_static_state_;
  S21Matrix cave_field_;
  MatrixReaderWriter matrixReaderWriter;

  static constexpr int kMinimalLiveNeighborsLimit = 0;
  static constexpr int kMaximalLiveNeighborsLimit = 7;
  static constexpr int kDefaultBornLimit = 3;
  static constexpr int kDefaultDeathDimit = 3;
  static constexpr int kDefaultInitChance = 50;
  static constexpr int kMinimalFieldSize = 1;
  static constexpr int kMaximalFieldSize = 50;
  static constexpr int kMinimalInitChance = 0;
  static constexpr int kMaximalInitChance = 100;
};
}  // namespace s21

#endif  // SRC_MAZE_MODEL_CAVE_GENERATOR_CAVE_GENERATOR_H_
