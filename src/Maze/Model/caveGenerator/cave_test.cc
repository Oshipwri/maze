#include <gtest/gtest.h>

#include "cave_generator.h"

TEST(Cave, BornDeathLimit) {
  s21::CaveGenerator cave;
  cave.SetBornLimit(-1);
  ASSERT_EQ(cave.GetBornLimit(), 0);
  cave.SetBornLimit(10);
  ASSERT_EQ(cave.GetBornLimit(), 7);
  cave.SetDeathLimit(-1);
  ASSERT_EQ(cave.GetDeathLimit(), 0);
  cave.SetDeathLimit(10);
  ASSERT_EQ(cave.GetDeathLimit(), 7);
  ASSERT_FALSE(cave.GetIsStaticState());
}

TEST(Cave, EmptyField) {
  s21::CaveGenerator cave;
  s21::S21Matrix empty;
  empty.set_rows(4);
  empty.set_cols(4);
  s21::MatrixReaderWriter mrw;
  mrw.WriteFile("./Maze/Model/caveGenerator/test_empty_field.txt", empty);
  cave.OpenCaveField("./Maze/Model/caveGenerator/test_empty_field.txt");
  cave.SetBornLimit(5);
  cave.SetDeathLimit(3);
  cave.GenerateNextIteration();
  for (int i = 0; i < cave.GetVerticalSize(); i++)
    for (int j = 0; j < cave.GetHorizontalSize(); j++) {
      ASSERT_EQ(cave.GetCaveField()(i, j), 0);
    }

  ASSERT_EQ(cave.GetBornLimit(), 5);
  ASSERT_EQ(cave.GetDeathLimit(), 3);
}

TEST(Cave, RandomGeneration) {
  s21::CaveGenerator cave;
  cave.SetVerticalSize(10);
  cave.SetHorizontalSize(10);
  cave.SetInitChance(10);
  srand(time(NULL));
  cave.GenerateRandomField();
  int alive_cells = 0;
  for (int i = 0; i < cave.GetVerticalSize(); i++)
    for (int j = 0; j < cave.GetHorizontalSize(); j++) {
      alive_cells =
          cave.GetCaveField()(i, j) == 0 ? alive_cells : alive_cells + 1;
    }

  ASSERT_LE(alive_cells, 25);
  ASSERT_GE(alive_cells, 0);

  cave.SetInitChance(20);
  alive_cells = 0;
  cave.GenerateRandomField();
  for (int i = 0; i < cave.GetVerticalSize(); i++)
    for (int j = 0; j < cave.GetHorizontalSize(); j++) {
      alive_cells =
          cave.GetCaveField()(i, j) == 0 ? alive_cells : alive_cells + 1;
    }
  ASSERT_LE(alive_cells, 35);
  ASSERT_GE(alive_cells, 5);

  cave.SetInitChance(80);
  alive_cells = 0;
  cave.GenerateRandomField();
  for (int i = 0; i < cave.GetVerticalSize(); i++)
    for (int j = 0; j < cave.GetHorizontalSize(); j++) {
      alive_cells =
          cave.GetCaveField()(i, j) == 0 ? alive_cells : alive_cells + 1;
    }
  ASSERT_LE(alive_cells, 95);
  ASSERT_GE(alive_cells, 65);
  ASSERT_EQ(cave.GetInitChance(), 80);
}

TEST(Cave, Iterations) {
  s21::CaveGenerator cave;
  s21::S21Matrix first;
  s21::MatrixReaderWriter mrw;
  first.set_rows(4);
  first.set_cols(4);
  first(0, 2) = 1;
  first(1, 1) = 1;
  first(1, 2) = 1;
  mrw.WriteFile("./Maze/Model/caveGenerator/test_iterations.txt", first);

  cave.OpenCaveField("./Maze/Model/caveGenerator/test_iterations.txt");
  cave.SetBornLimit(2);
  cave.SetDeathLimit(2);
  cave.GenerateNextIteration();
  s21::S21Matrix next;
  next.set_rows(4);
  next.set_cols(4);
  next(0, 1) = 1;
  next(0, 2) = 1;
  next(1, 1) = 1;
  next(1, 2) = 1;
  cave.SaveCaveField("./Maze/Model/caveGenerator/iterations.txt");
  for (int i = 0; i < cave.GetVerticalSize(); i++)
    for (int j = 0; j < cave.GetHorizontalSize(); j++) {
      ASSERT_EQ(cave.GetCaveField()(i, j), next(i, j));
    }
}
