#include "cave_generator.h"

namespace s21 {
CaveGenerator::CaveGenerator() {
  born_limit_ = kDefaultBornLimit;
  death_limit_ = kDefaultDeathDimit;
  init_chance_ = kDefaultInitChance;
  is_static_state_ = false;
}

int CaveGenerator::GetBornLimit() { return born_limit_; }

void CaveGenerator::SetBornLimit(int limit) {
  is_static_state_ = false;
  int born_limit = limit;
  if (born_limit < kMinimalLiveNeighborsLimit) {
    born_limit = kMinimalLiveNeighborsLimit;
  } else if (born_limit > kMaximalLiveNeighborsLimit) {
    born_limit = kMaximalLiveNeighborsLimit;
  }
  born_limit_ = born_limit;
}

int CaveGenerator::GetDeathLimit() { return death_limit_; }

void CaveGenerator::SetDeathLimit(int limit) {
  is_static_state_ = false;
  int death_limit = limit;
  if (death_limit < kMinimalLiveNeighborsLimit) {
    death_limit = kMinimalLiveNeighborsLimit;
  } else if (death_limit > kMaximalLiveNeighborsLimit) {
    death_limit = kMaximalLiveNeighborsLimit;
  }
  death_limit_ = death_limit;
}

void CaveGenerator::SetInitChance(int chance) {
  int init_chance = chance;
  if (init_chance < kMinimalInitChance) {
    init_chance = kMinimalInitChance;
  } else if (init_chance > kMaximalInitChance) {
    init_chance = kMaximalInitChance;
  }
  init_chance_ = init_chance;
}

int CaveGenerator::GetInitChance() { return init_chance_; }

bool CaveGenerator::GetIsStaticState() { return is_static_state_; }

void CaveGenerator::SaveCaveField(const std::string& filepath) {
  matrixReaderWriter.WriteFile(filepath, cave_field_);
}

void CaveGenerator::OpenCaveField(const std::string& filepath) {
  is_static_state_ = false;
  matrixReaderWriter.ReadFile(filepath, cave_field_);
  if (cave_field_.get_rows() > kMaximalFieldSize)
    cave_field_.set_rows(kMaximalFieldSize);

  if (cave_field_.get_cols() > kMaximalFieldSize)
    cave_field_.set_cols(kMaximalFieldSize);
}

void CaveGenerator::GenerateNextIteration() {
  if (!is_static_state_) {
    is_static_state_ = true;
    S21Matrix cave_copy(cave_field_);
    for (int row = 0; row < cave_field_.get_rows(); row++)
      for (int col = 0; col < cave_field_.get_cols(); col++) {
        FindNextCellState(cave_copy, row, col);
      }
    cave_field_ = cave_copy;
  }
}

void CaveGenerator::FindNextCellState(S21Matrix& cave_copy, int row, int col) {
  int min_row = row == 0 ? row : row - 1;
  int max_row = row == cave_field_.get_rows() - 1 ? row : row + 1;
  int min_col = col == 0 ? col : col - 1;
  int max_col = col == cave_field_.get_cols() - 1 ? col : col + 1;
  int neighbors = 0;
  for (int i = min_row; i <= max_row; i++)
    for (int j = min_col; j <= max_col; j++) {
      if (cave_field_(i, j) != 0 && !(i == row && j == col)) neighbors++;
    }
  if (cave_field_(row, col) == 0 && neighbors > born_limit_) {
    is_static_state_ = false;
    cave_copy(row, col) = 1;
  } else if (cave_field_(row, col) != 0 && neighbors < death_limit_) {
    is_static_state_ = false;
    cave_copy(row, col) = 0;
  }
}

void CaveGenerator::GenerateRandomField() {
  is_static_state_ = false;
  for (int row = 0; row < cave_field_.get_rows(); row++)
    for (int col = 0; col < cave_field_.get_cols(); col++) {
      cave_field_(row, col) = GenerateDeadOrAlive();
    }
}

int CaveGenerator::GenerateDeadOrAlive() {
  int result;
  int random_number = rand() % 100;
  if (random_number <= init_chance_)
    result = 1;
  else
    result = 0;
  return result;
}

void CaveGenerator::SetVerticalSize(int size) {
  is_static_state_ = false;
  cave_field_.set_rows(size);
}

void CaveGenerator::SetHorizontalSize(int size) {
  is_static_state_ = false;
  cave_field_.set_cols(size);
}

int CaveGenerator::GetVerticalSize() { return cave_field_.get_rows(); }

int CaveGenerator::GetHorizontalSize() { return cave_field_.get_cols(); }
}  // namespace s21
