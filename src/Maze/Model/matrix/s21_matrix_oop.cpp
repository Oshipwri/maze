#include "s21_matrix_oop.h"

namespace s21 {
S21Matrix::S21Matrix() {
  _rows = kDefaultRows;
  _cols = kDefaultCols;
  allocate_memory();
}

S21Matrix::S21Matrix(int rows, int cols) {
  if (rows <= 0 || cols <= 0) {
    throw std::out_of_range("Rows & cols number must be greater than zero");
  } else {
    _rows = rows;
    _cols = cols;
    allocate_memory();
  }
}

S21Matrix::S21Matrix(const S21Matrix& other) { copy_matrix(other); }

S21Matrix::S21Matrix(S21Matrix&& other) {
  _rows = other._rows;
  _cols = other._cols;
  this->_matrix = other._matrix;
  other._matrix = nullptr;
}

S21Matrix::~S21Matrix() {
  if (_matrix != nullptr) {
    for (int i = 0; i < _rows; i++) {
      delete[] _matrix[i];
    }
    delete[] _matrix;
    _matrix = nullptr;
    _rows = 0;
    _cols = 0;
  }
}

bool S21Matrix::eq_matrix(const S21Matrix& other) {
  bool result = false;
  if (_rows == other._rows && _cols == other._cols) {
    result = true;
    for (int i = 0; i < _rows; i++) {
      for (int j = 0; j < _cols; j++)
        if (fabs(_matrix[i][j] - other._matrix[i][j]) > kEpsilon) {
          result = false;
          break;
        }
      if (!result) break;
    }
  }
  return result;
}

void S21Matrix::calculate(const S21Matrix& other, char operation) {
  if (_rows != other._rows || _cols != other._cols) {
    throw std::out_of_range("Rows & cols number must be equal in matrices");
  } else {
    for (int i = 0; i < _rows; i++)
      for (int j = 0; j < _cols; j++)
        if (operation == '+')
          _matrix[i][j] += other._matrix[i][j];
        else if (operation == '-')
          _matrix[i][j] -= other._matrix[i][j];
  }
}

void S21Matrix::mul_number(const double num) {
  for (int i = 0; i < _rows; i++)
    for (int j = 0; j < _cols; j++) _matrix[i][j] = _matrix[i][j] * num;
}

void S21Matrix::mul_matrix(const S21Matrix& other) {
  if (_rows != other._cols) {
    throw std::out_of_range(
        "Rows in first matrix must be equal to columns in second matrix");
  } else {
    S21Matrix* result = new S21Matrix(_rows, other._cols);
    for (int i = 0; i < _rows; i++)
      for (int j = 0; j < other._cols; j++)
        for (int k = 0; k < _cols; k++)
          result->_matrix[i][j] += _matrix[i][k] * other._matrix[k][j];
    int temp_rows = _rows;
    this->~S21Matrix();
    this->_rows = temp_rows;
    this->_cols = other._cols;
    this->_matrix = result->_matrix;
    result->_matrix = nullptr;
    delete result;
  }
}

S21Matrix S21Matrix::transpose() {
  S21Matrix result(_cols, _rows);
  for (int i = 0; i < _rows; i++) {
    for (int j = 0; j < _cols; j++) {
      result._matrix[j][i] = _matrix[i][j];
    }
  }
  return result;
}

S21Matrix S21Matrix::calc_complements() {
  if (_rows != _cols || _rows == 1) {
    throw std::out_of_range("Rows must be equal to cols and greater then 1");
  } else {
    S21Matrix result(_rows, _cols);
    for (int i = 0; i < _rows; i++) {
      for (int j = 0; j < _cols; j++) {
        result._matrix[i][j] = cofactor(i, j);
      }
    }
    return result;
  }
}

double S21Matrix::determinant() {
  if (_rows != _cols) {
    throw std::out_of_range("Rows must be equal to cols");
  } else {
    double result;
    int n = _rows;
    if (n == 1) {
      result = _matrix[0][0];
    } else {
      result = 0;
      for (int k = 0; k < n; k++) {
        result += _matrix[0][k] * cofactor(0, k);
      }
    }
    return result;
  }
}

S21Matrix S21Matrix::inverse_matrix() {
  if (_rows != _cols || fabs(determinant()) < kEpsilon) {
    throw std::out_of_range(
        "Rows is not equal to cols or determinant is equal to 0");
  } else {
    double number = 1 / determinant();
    S21Matrix result(_rows, _cols);
    if (_rows == 1) {
      result(0, 0) = number;
    } else {
      S21Matrix compliment_matrix = calc_complements();
      result = compliment_matrix.transpose();
      result.mul_number(number);
    }
    return result;
  }
}

S21Matrix S21Matrix::operator+(const S21Matrix& other) {
  S21Matrix result(*this);
  result.sum_matrix(other);
  return result;
}

S21Matrix S21Matrix::operator-(const S21Matrix& other) {
  S21Matrix result(*this);
  result.sub_matrix(other);
  return result;
}

S21Matrix S21Matrix::operator*(const S21Matrix& other) {
  S21Matrix result(*this);
  result.mul_matrix(other);
  return result;
}

S21Matrix operator*(const S21Matrix& matrix, double num) {
  S21Matrix result(matrix);
  result.mul_number(num);
  return result;
}

S21Matrix operator*(double num, const S21Matrix& matrix) {
  return matrix * num;
}

bool S21Matrix::operator==(const S21Matrix& other) {
  return this->eq_matrix(other);
}

void S21Matrix::operator=(const S21Matrix& other) {
  this->~S21Matrix();
  copy_matrix(other);
}

void S21Matrix::operator+=(const S21Matrix& other) { this->sum_matrix(other); }

void S21Matrix::operator-=(const S21Matrix& other) { this->sub_matrix(other); }

void S21Matrix::operator*=(const S21Matrix& other) { this->mul_matrix(other); }

void S21Matrix::operator*=(const double num) { this->mul_number(num); }

double& S21Matrix::operator()(int i, int j) {
  if (i >= _rows || j >= _cols || i < 0 || j < 0) {
    throw std::out_of_range("Row or column is out of range");
  } else {
    return _matrix[i][j];
  }
}

double S21Matrix::cofactor(int row, int column) {
  double result;
  S21Matrix reduced_matrix = get_reduced_for_minor(row, column);
  result = pow(-1, column + row) * reduced_matrix.determinant();
  return result;
}

S21Matrix S21Matrix::get_reduced_for_minor(int row, int column) {
  int n = _rows;
  S21Matrix result(n - 1, n - 1);
  int _i = 0;
  for (int i = 0; i < n; i++) {
    if (i == row) {
      continue;
    }
    int _j = 0;
    for (int j = 0; j < n; j++) {
      if (j == column) {
        continue;
      }
      result._matrix[_i][_j] = _matrix[i][j];
      _j++;
    }
    _i++;
  }
  return result;
}

void S21Matrix::sum_matrix(const S21Matrix& other) {
  this->calculate(other, '+');
}

void S21Matrix::sub_matrix(const S21Matrix& other) {
  this->calculate(other, '-');
}

int S21Matrix::get_rows() { return _rows; }

int S21Matrix::get_cols() { return _cols; }

double** S21Matrix::get_matrix() { return _matrix; }

void S21Matrix::set_rows(int rows) {
  if (rows <= 0) {
    throw std::out_of_range("Rows number must be more than zero");
  } else {
    S21Matrix temp(*this);
    int temp_rows = _rows;
    int temp_cols = _cols;
    this->~S21Matrix();
    int rest_rows = temp_rows > rows ? rows : temp_rows;
    _rows = rows;
    _cols = temp_cols;
    allocate_memory();
    for (int i = 0; i < rest_rows; i++) {
      for (int j = 0; j < _cols; j++) {
        _matrix[i][j] = temp(i, j);
      }
    }
  }
}

void S21Matrix::set_cols(int cols) {
  if (cols <= 0) {
    throw std::out_of_range("Cols number must be more than zero");
  } else {
    S21Matrix temp(*this);
    int temp_rows = _rows;
    int temp_cols = _cols;
    this->~S21Matrix();
    int rest_cols = temp_cols > cols ? cols : temp_cols;
    _rows = temp_rows;
    _cols = cols;
    allocate_memory();
    for (int i = 0; i < _rows; i++) {
      for (int j = 0; j < rest_cols; j++) {
        _matrix[i][j] = temp(i, j);
      }
    }
  }
}

void S21Matrix::allocate_memory() {
  _matrix = new double*[_rows];
  for (int i = 0; i < _rows; i++) {
    _matrix[i] = new double[_cols];
    memset(_matrix[i], 0.0, sizeof(double) * _cols);
  }
}

void S21Matrix::copy_matrix(const S21Matrix& other) {
  if (other._matrix == nullptr) {
    throw std::invalid_argument("Matrix wasn't initialized");
  } else {
    _rows = other._rows;
    _cols = other._cols;
    allocate_memory();
    for (int i = 0; i < _rows; i++)
      for (int j = 0; j < _cols; j++) _matrix[i][j] = other._matrix[i][j];
  }
}
}  // namespace s21
