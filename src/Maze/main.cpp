#include <QApplication>

#include "View/mainwindow.h"

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  s21::Model model;
  s21::Controller controller(&model);
  MainWindow window(&controller);
  window.show();
  return app.exec();
}
