#include "mainwindow.h"

#include "./ui_mainwindow.h"

MainWindow::MainWindow(s21::Controller *controller, QWidget *parent)
    : controller_(controller), QMainWindow(parent), ui(new Ui::MainWindow) {
  timer_ = new QTimer();
  ui->setupUi(this);
  ui->widgetCave->SetController(controller);
  ui->widgetMaze->SetController(controller);
  connect(timer_, SIGNAL(timeout()), this,
          SLOT(on_pushButtonCaveNext_clicked()));
}

MainWindow::~MainWindow() {
  delete ui;
  delete timer_;
}

void MainWindow::on_pushButtonCaveGenerate_clicked() {
  controller_->SetCaveHorizontallSize(ui->spinBoxCaveHorizontalSize->value());
  controller_->SetCaveVerticalSize(ui->spinBoxCaveVerticalSize->value());
  controller_->SetBornLimit(ui->spinBoxCaveBirthLimit->value());
  controller_->SetDeathLimit(ui->spinBoxCaveDeathLimit->value());
  controller_->SetInitChance(ui->spinBoxCaveInitChance->value());
  controller_->GenerateCave();
  ui->widgetCave->repaint();
}

void MainWindow::on_pushButtonMazeGenerate_clicked() {
  controller_->SetMazeVerticalSize(ui->spinBoxMazeVerticalSize->value());
  controller_->SetMazeHorizontalSize(ui->spinBoxMazeHorizontalSize->value());
  controller_->GenerateMaze();
  ui->widgetMaze->ResetCells();
  ui->widgetMaze->repaint();
}

void MainWindow::on_pushButtonCaveNext_clicked() {
  if (!timer_->isActive()) {
    controller_->SetBornLimit(ui->spinBoxCaveBirthLimit->value());
    controller_->SetDeathLimit(ui->spinBoxCaveDeathLimit->value());
  }
  controller_->GenerateCaveNextIteration();
  ui->widgetCave->repaint();
  if (timer_->isActive() && controller_->IsCaveStaticState()) {
    timer_->stop();
    ui->pushButtonCaveAuto->setText("Auto >>");
    EnableButtons();
  }
}

void MainWindow::on_pushButtonCaveAuto_clicked() {
  if (timer_->isActive()) {
    timer_->stop();
    ui->pushButtonCaveAuto->setText("Auto >>");
  } else {
    controller_->SetBornLimit(ui->spinBoxCaveBirthLimit->value());
    controller_->SetDeathLimit(ui->spinBoxCaveDeathLimit->value());
    timer_->start(ui->spinBoxCaveTimerInterval->value());
    ui->pushButtonCaveAuto->setText("Stop");
  }
  EnableButtons();
}

void MainWindow::EnableButtons() {
  auto mode = !timer_->isActive();
  ui->pushButtonCaveGenerate->setEnabled(mode);
  ui->pushButtonCaveLoad->setEnabled(mode);
  ui->pushButtonCaveSave->setEnabled(mode);
  ui->pushButtonCaveNext->setEnabled(mode);
  ui->spinBoxCaveTimerInterval->setEnabled(mode);
  ui->spinBoxCaveHorizontalSize->setEnabled(mode);
  ui->spinBoxCaveVerticalSize->setEnabled(mode);
  ui->spinBoxCaveBirthLimit->setEnabled(mode);
  ui->spinBoxCaveDeathLimit->setEnabled(mode);
  ui->spinBoxCaveInitChance->setEnabled(mode);
}

void MainWindow::on_pushButtonCaveSave_clicked() {
  QString q_filename =
      QFileDialog::getSaveFileName(this, "Save file", ".", "txt files (*.txt)");
  if (!q_filename.isEmpty()) {
    std::string filename = q_filename.toStdString();
    controller_->SaveCaveField(filename);
  }
}

void MainWindow::on_pushButtonCaveLoad_clicked() {
  QString q_filename = QFileDialog::getOpenFileName(this, "Select file", ".",
                                                    "txt files (*.txt)");
  if (!q_filename.isEmpty()) {
    std::string filename = q_filename.toStdString();
    try {
      controller_->LoadCaveField(filename);
      ui->spinBoxCaveHorizontalSize->setValue(
          controller_->GetCaveHorizontalSize());
      ui->spinBoxCaveVerticalSize->setValue(controller_->GetCaveVerticalSize());
      ui->widgetCave->repaint();
    } catch (...) {
      ShowWarning();
    }
  }
}

void MainWindow::on_pushButtonMazeLoad_clicked() {
  QString q_filename = QFileDialog::getOpenFileName(this, "Select file", ".",
                                                    "txt files (*.txt)");
  if (!q_filename.isEmpty()) {
    std::string filename = q_filename.toStdString();
    try {
      controller_->LoadMazeField(filename);
      ui->spinBoxMazeHorizontalSize->setValue(
          controller_->GetMazeHorizontalSize());
      ui->spinBoxMazeVerticalSize->setValue(controller_->GetMazeVerticalSize());
      ui->widgetMaze->ResetCells();
      ui->widgetMaze->repaint();
    } catch (...) {
      ShowWarning();
    }
  }
}

void MainWindow::on_pushButtonMazeSave_clicked() {
  QString q_filename =
      QFileDialog::getSaveFileName(this, "Save file", ".", "txt files (*.txt)");
  if (!q_filename.isEmpty()) {
    std::string filename = q_filename.toStdString();
    controller_->SaveMazeField(filename);
  }
}

void MainWindow::ShowWarning() {
  QMessageBox msgBox;
  msgBox.setWindowTitle("Error");
  msgBox.setText("Incorrect file!");
  msgBox.exec();
}
