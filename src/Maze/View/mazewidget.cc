#include "mazewidget.h"

#include <QMouseEvent>

#include "ui_mazewidget.h"

MazeWidget::MazeWidget(QWidget *parent)
    : QWidget(parent), ui(new Ui::MazeWidget) {
  ui->setupUi(this);
}

MazeWidget::~MazeWidget() { delete ui; }

void MazeWidget::paintEvent(QPaintEvent *event) {
  Q_UNUSED(event);
  QPainter painter(this);
  DrawBorders(&painter);
  DrawStartEndPoints(&painter);
}

void MazeWidget::mousePressEvent(QMouseEvent *event) {
  if (event->button() == Qt::LeftButton) {
    if (cells_.size() == 2) cells_.clear();
    PushCell(event->pos());
    update();
  }
}

void MazeWidget::DrawBorders(QPainter *painter) {
  painter->setPen(QPen(Qt::black, kWidthLine, Qt::SolidLine, Qt::RoundCap));
  auto borders = controller_->GetMazeField();
  auto vertical = borders.first;
  auto horizontal = borders.second;
  auto width = controller_->GetMazeHorizontalSize();
  auto height = controller_->GetMazeVerticalSize();
  auto cell_width = kMaxSize / width;
  auto cell_heigh = kMaxSize / height;
  painter->drawLine(kWidthLine / 2, kWidthLine / 2, kWidthLine / 2,
                    cell_heigh * height);
  painter->drawLine(kWidthLine / 2, kWidthLine / 2, cell_width * width,
                    kWidthLine / 2);

  for (auto i = 0; i < width; i++) {
    for (auto j = 0; j < height; j++) {
      if (vertical(j, i) != 0) {
        painter->drawLine(cell_width * (i + 1), cell_heigh * j,
                          cell_width * (i + 1), cell_heigh * (j + 1));
      }
      if (horizontal(j, i) != 0) {
        painter->drawLine(cell_width * i, cell_heigh * (j + 1),
                          cell_width * (i + 1), cell_heigh * (j + 1));
      }
    }
  }
}

void MazeWidget::DrawStartEndPoints(QPainter *painter) {
  if (cells_.size() > 0) {
    painter->setPen(QPen(Qt::red, kWidthLine, Qt::SolidLine, Qt::RoundCap));
    painter->setBrush(QBrush(Qt::red, Qt::SolidPattern));
    auto cell_width = kMaxSize / controller_->GetMazeHorizontalSize();
    auto cell_heigh = kMaxSize / controller_->GetMazeVerticalSize();
    for (auto point : cells_) {
      painter->drawRect(
          point.first * cell_width + cell_width / 2 - kWidthLine / 2,
          point.second * cell_heigh + cell_heigh / 2 - kWidthLine / 2,
          kWidthLine, kWidthLine);
    }
    if (cells_.size() == 2) DrawWay(painter);
  }
}

void MazeWidget::PushCell(const QPoint &point) {
  auto cell_width = kMaxSize / controller_->GetMazeHorizontalSize();
  auto cell_heigh = kMaxSize / controller_->GetMazeVerticalSize();
  auto x = point.x() / cell_width;
  auto y = point.y() / cell_heigh;
  cells_.push_back(std::make_pair(x, y));
}

void MazeWidget::DrawWay(QPainter *painter) {
  painter->setPen(QPen(Qt::red, kWidthLine, Qt::SolidLine, Qt::RoundCap));
  auto way = controller_->GetMazeWay(cells_[0].second, cells_[0].first,
                                     cells_[1].second, cells_[1].first);
  if (way.size() < 2) return;

  auto cell_width = kMaxSize / controller_->GetMazeHorizontalSize();
  auto cell_heigh = kMaxSize / controller_->GetMazeVerticalSize();

  for (auto i = 1; i < way.size(); i++) {
    auto begin = way[i - 1];
    auto end = way[i];
    painter->drawLine(cell_width / 2 + cell_width * begin.second,
                      cell_heigh / 2 + cell_heigh * begin.first,
                      cell_width / 2 + cell_width * end.second,
                      cell_heigh / 2 + cell_heigh * end.first);
  }
}

void MazeWidget::ResetCells() { cells_.clear(); }
