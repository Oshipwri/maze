#ifndef SRC_MAZE_VIEW_CAVEWIDGET_H
#define SRC_MAZE_VIEW_CAVEWIDGET_H

#include <QPainter>
#include <QWidget>

#include "../Controller/controller.h"

namespace Ui {
class CaveWidget;
}

class CaveWidget : public QWidget {
  Q_OBJECT

 public:
  explicit CaveWidget(QWidget *parent = nullptr);
  ~CaveWidget();
  void SetController(s21::Controller *controller) { controller_ = controller; };

 protected:
  void paintEvent(QPaintEvent *event) override;
  static const int kMaxSize = 500;

 private:
  Ui::CaveWidget *ui;
  s21::Controller *controller_;
};

#endif  // SRC_MAZE_VIEW_CAVEWIDGET_H
