#ifndef SRC_MAZE_VIEW_MAINWINDOW_H
#define SRC_MAZE_VIEW_MAINWINDOW_H

#include <QFileDialog>
#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>

#include "../Controller/controller.h"

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  MainWindow(s21::Controller *controller, QWidget *parent = nullptr);
  ~MainWindow();

 private slots:
  void on_pushButtonCaveGenerate_clicked();
  void on_pushButtonMazeGenerate_clicked();
  void on_pushButtonCaveNext_clicked();
  void on_pushButtonCaveAuto_clicked();
  void on_pushButtonCaveSave_clicked();
  void on_pushButtonCaveLoad_clicked();
  void on_pushButtonMazeLoad_clicked();
  void on_pushButtonMazeSave_clicked();

 private:
  Ui::MainWindow *ui;
  s21::Controller *controller_;
  QTimer *timer_;
  const int kMaxSizeArea = 500;
  void EnableButtons();
  void ShowWarning();
};
#endif  // SRC_MAZE_VIEW_MAINWINDOW_H
