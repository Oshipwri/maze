#ifndef SRC_MAZE_VIEW_MAZEWIDGET_H
#define SRC_MAZE_VIEW_MAZEWIDGET_H

#include <QPainter>
#include <QWidget>
#include <vector>

#include "../Controller/controller.h"

namespace Ui {
class MazeWidget;
}

class MazeWidget : public QWidget {
  Q_OBJECT

 public:
  explicit MazeWidget(QWidget *parent = nullptr);
  ~MazeWidget();
  void SetController(s21::Controller *controller) { controller_ = controller; };
  void ResetCells();

 protected:
  void paintEvent(QPaintEvent *event) override;
  void mousePressEvent(QMouseEvent *event) override;
  static const int kMaxSize = 500;
  static const int kWidthLine = 2;

 private:
  Ui::MazeWidget *ui;
  s21::Controller *controller_;
  std::vector<std::pair<int, int>> cells_;
  void DrawBorders(QPainter *painter);
  void DrawField(QPainter *painter);
  void DrawStartEndPoints(QPainter *painter);
  void DrawWay(QPainter *painter);
  void PushCell(const QPoint &point);
};

#endif  // SRC_MAZE_VIEW_MAZEWIDGET_H
