#include "cavewidget.h"

#include "../Model/matrix/s21_matrix_oop.h"
#include "ui_cavewidget.h"

CaveWidget::CaveWidget(QWidget *parent)
    : QWidget(parent), ui(new Ui::CaveWidget) {
  ui->setupUi(this);
}

CaveWidget::~CaveWidget() { delete ui; }

void CaveWidget::paintEvent(QPaintEvent *event) {
  Q_UNUSED(event);
  QPainter painter(this);
  auto width = controller_->GetCaveHorizontalSize();
  auto height = controller_->GetCaveVerticalSize();
  auto cell_width = kMaxSize / width;
  auto cell_height = kMaxSize / height;
  painter.setBrush(QBrush(Qt::black, Qt::SolidPattern));
  s21::S21Matrix cave = controller_->GetCaveField();
  for (auto i = 0; i < width; i++) {
    for (auto j = 0; j < height; j++) {
      if (cave(j, i) != 0) {
        painter.drawRect(i * cell_width, j * cell_height, cell_width,
                         cell_height);
      }
    }
  }
}
