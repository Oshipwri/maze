#ifndef SRC_MAZE_CONTROLLER_CONTROLLER_H
#define SRC_MAZE_CONTROLLER_CONTROLLER_H

#include <utility>
#include <vector>

#include "../Model/model.h"

namespace s21 {
class Controller {
 public:
  explicit Controller(s21::Model* model) : model_(model){};
  void SetMazeVerticalSize(int size) { model_->SetMazeVerticalSize(size); };
  void SetMazeHorizontalSize(int size) { model_->SetMazeHorizontalSize(size); };
  void SetCaveVerticalSize(int size) { model_->SetCaveVerticalSize(size); };
  void SetCaveHorizontallSize(int size) {
    model_->SetCaveHorizontalSize(size);
  };
  void SetBornLimit(int limit) { model_->SetBornLimit(limit); };
  void SetDeathLimit(int limit) { model_->SetDeathLimit(limit); };
  void SetInitChance(int chance) { model_->SetInitChance(chance); };
  void GenerateCave() { model_->GenerateCave(); };
  void GenerateMaze() { model_->GenerateMaze(); };
  std::pair<S21Matrix, S21Matrix> GetMazeField();
  int GetMazeVerticalSize() { return model_->GetMazeVerticalSize(); };
  int GetMazeHorizontalSize() { return model_->GetMazeHorizontalSize(); };
  int GetCaveVerticalSize() { return model_->GetCaveVerticalSize(); };
  int GetCaveHorizontalSize() { return model_->GetCaveHorizontalSize(); };
  S21Matrix GetCaveField() { return model_->GetCaveField(); };
  void GenerateCaveNextIteration() { model_->GenerateCaveNextIteration(); };
  void SaveCaveField(const std::string& filepath) {
    model_->SaveCaveField(filepath);
  };
  void LoadCaveField(const std::string& filepath) {
    model_->LoadCaveField(filepath);
  };
  void SaveMazeField(const std::string& filepath) {
    model_->SaveMazeField(filepath);
  };
  void LoadMazeField(const std::string& filepath) {
    model_->LoadMazeField(filepath);
  };
  bool IsCaveStaticState() { return model_->IsCaveStaticState(); };
  std::vector<std::pair<int, int>> GetMazeWay(int start_row, int start_col,
                                              int finish_row, int finish_col) {
    return model_->GetMazeWay(start_row, start_col, finish_row, finish_col);
  }

 private:
  s21::Model* model_;
};

}  // namespace s21
#endif  // SRC_MAZE_CONTROLLER_CONTROLLER_H
