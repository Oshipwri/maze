#include "controller.h"

std::pair<s21::S21Matrix, s21::S21Matrix> s21::Controller::GetMazeField() {
  return model_->GetMazeField();
}
