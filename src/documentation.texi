\input texinfo
@settitle Maze

@copying
Copyright @copyright{} 2022-09 mcarb@@student.21-school.ru fscourge@@student.21-school.ru oshipwri@@student.21-school.ru
@end copying

@titlepage
@title Maze
@author mcarb, fscourge, oshipwri 
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage

The generation of ideal labyrinths and caves is implemented in this project.

@itemize @bullet
@item First page: Generation and solving of a perfect maze.


Before generation you need set vertical and horizontal size of maze.
Then press button "Genetate".

For solving maze you need to click on the maze field to set the start and end of the route.

Also you can load or save the maze to a file. Use buttons "Load" and "Save" to do this.

@item Second page: Cave Generation.

Before generation you need set:
@itemize @bullet
@item Vertical and horizontal size of cave.
@item Limits for "birth" and "death" of a cell
@item Chance for the starting initialization of the cell
@end itemize

Then press button "Genetate".

Press button "Next" for rendering the next iteration of the algorithm.

Press button "Auto" for automatic rendering the next iterations of the algorithm.

Also you can load or save the cave to a file. Use buttons "Load" and "Save" to do this.

@end itemize


Created by: mcarb, fscourge, oshipwri 

September, 2022
@bye