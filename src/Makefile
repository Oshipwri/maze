TEXI2DVI = makeinfo --html
SRC_DIR = Maze/
BUILD_DIR = build/
CC = g++
STD = -std=c++17
CFLAG = -Wall -Wextra -Wshadow -pedantic -g -fsanitize=address

GTEST_LIBS = -lgtest -pthread
INCLUDE_PATHS = ./Maze
INCLUDE = $(addprefix -I,$(INCLUDE_PATHS))

TEST_FILES = $(addprefix ./$(SRC_DIR)/Model/,\
	model_test.cc\
	matrixReaderWriter/matrix_reader_writer_test.cc\
	matrixReaderWriter/matrix_reader_writer.cc\
	matrix/s21_matrix_oop.cpp\
	caveGenerator/cave_test.cc\
	caveGenerator/cave_generator.cc\
	mazeGenerator/maze_test.cc\
	mazeGenerator/maze_generator.cc\
	)



OS := $(shell uname -s)

ifeq ($(OS), Darwin)
	APP = Maze.app
else
	APP = Maze
endif


all: install

uninstall:
	@rm -rf $(BUILD_DIR)

install: uninstall
	@mkdir $(BUILD_DIR)
	@cmake $(SRC_DIR)CMakeLists.txt 
	@make -C $(SRC_DIR)
	@cp -r $(SRC_DIR)$(APP) $(BUILD_DIR)/$(APP)
	@make clean -C $(SRC_DIR)

dvi:
	$(TEXI2DVI) ./documentation.texi

dist: 
	@cmake $(SRC_DIR)CMakeLists.txt 
	@make -C $(SRC_DIR) 
	@tar -zcf Maze.tar $(SRC_DIR)$(APP) 
	@make clean -C $(SRC_DIR)

test: clean
	$(CC) $(STD) $(CFLAG) -o test $(TEST_FILES) $(INCLUDE) $(GTEST_LIBS)
	./test

gcov_report: CFLAG+=--coverage
gcov_report: test
	@./test
	lcov -t "test" -o test.info --no-external -c -d .
	lcov --remove test.info '*/Maze/Model/matrix/*' -o test.info
	genhtml -o report test.info
	open report/index.html


clean:
	@rm -f test
	@rm -f ./Maze/.clang-format
	@rm -rf *.o *.gcno *.gcda *.gcov test.info report
	@rm -rf ./Maze/Model/matrixReaderWriter/*.txt
	@rm -rf ./Maze/Model/caveGenerator/*.txt
	@rm -rf ./Maze/Model/mazeGenerator/test_load_save.txt

clang:
	@cp ../materials/linters/.clang-format Maze/
	@clang-format -n Maze/View/*.cc Maze/View/*.h
	@clang-format -n Maze/Controller/*.cc Maze/Controller/*.h
	@clang-format -n Maze/Model/*.cc Maze/Model/*.h
	@clang-format -n Maze/Model/matrixReaderWriter/*.cc Maze/Model/matrixReaderWriter/*.h
	@clang-format -n Maze/Model/caveGenerator/*.cc Maze/Model/caveGenerator/*.h
	@clang-format -n Maze/Model/mazeGenerator/*.cc Maze/Model/mazeGenerator/*.h
	
cppcheck:
	@cppcheck --enable=all  --check-config --suppress=missingInclude Maze/.
